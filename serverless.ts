import type { Serverless } from 'serverless/aws';

const serverlessConfiguration: Serverless = {
  service: {
    name: 'typescript-serverless-nestjs'
  },
  provider: {
    name: 'aws',
    runtime: 'nodejs12.x',
    apiGateway: {
      minimumCompressionSize: 1024,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
    },
  },
  frameworkVersion: '2',
  custom: {
    webpack: {
      webpackConfig: './webpack.config.js',
      includeModules: true
    }
  },
  plugins: [
    'serverless-webpack',
    'serverless-plugin-optimize',
    'serverless-offline'
    ],
  functions: {
    main: {
      handler: 'src/lambda.handler',
      events: [
        {
          http: {
            method: 'any',
            path: '/{any+}',
          }
        }
      ]
    }
  }
}

module.exports = serverlessConfiguration;
