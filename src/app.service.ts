import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hola mundo !!!';
  }

  setHello(msg: string): string {
    return msg;
  }
}
